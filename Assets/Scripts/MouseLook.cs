using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class MouseLook : MonoBehaviour
{
    public enum RotationAxes
    {
        MouseXandY = 0,
        MouseX = 1,
        MouseY = 2
    }
    public RotationAxes axes = RotationAxes.MouseXandY;
    public float sensitivityHor = 9.0f; 
    public float sensitivityVer = 9.0f;

    public float minimumVert = -45.0f;
    public float maximumVert = 45.0f;
    public float minimumHor = -45.0f;
    public float maximumHor = 45.0f;

    private float _verticalRot = 0.0f;
    private float _horizontalRot = 0.0f;
    
    // Start is called before the first frame update
    void Start()
    {
        // Freeze the rotation so that the player object isn't affected by the physics simulation
        Rigidbody body = GetComponent<Rigidbody>();
        if (body != null)
        {
            body.freezeRotation = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (axes == RotationAxes.MouseX)
        {
            _horizontalRot += Input.GetAxis("Mouse X") * sensitivityHor;
            _horizontalRot = Mathf.Clamp(_horizontalRot, minimumHor, maximumHor);
            _verticalRot = transform.localEulerAngles.x;
            transform.localEulerAngles = new Vector3(_verticalRot, _horizontalRot, 0);
        } else if (axes == RotationAxes.MouseY)
        {
            _verticalRot -= Input.GetAxis("Mouse Y") * sensitivityVer;
            _verticalRot = Mathf.Clamp(_verticalRot, minimumVert, maximumVert);
            _horizontalRot = transform.localEulerAngles.y;
            transform.localEulerAngles = new Vector3(_verticalRot, _horizontalRot, 0);
        }
        else
        {
            _horizontalRot += Input.GetAxis("Mouse X") * sensitivityHor;
            _horizontalRot = Mathf.Clamp(_horizontalRot, minimumHor, maximumHor);
            _verticalRot -= Input.GetAxis("Mouse Y") * sensitivityVer;
            _verticalRot = Mathf.Clamp(_verticalRot, minimumVert, maximumVert);
            transform.localEulerAngles = new Vector3(_verticalRot, _horizontalRot, 0);
        }
    }
}
