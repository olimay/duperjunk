using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Junkyard : MonoBehaviour
{
    private Dictionary<JunkColor, JunkGraph> _junkGraphs;
    public int phase;

    private void Start()
    {
        string junkyardStartMessage = "Junkyard initialized!";
        Debug.Log(junkyardStartMessage);
    }

    public void AddJunk(Junk junk)
    {
        if (_junkGraphs == null)
        {
            _junkGraphs = new Dictionary<JunkColor, JunkGraph>();
        }
        _junkGraphs[junk.color].AddJunk(junk);
    }

    public void RemoveJunk(Junk junk)
    {
        if (_junkGraphs == null)
        {
            // error
            return;
        }
        if (!_junkGraphs.ContainsKey(junk.color))
        {
            // missing graph
            return;
        }
        _junkGraphs[junk.color].RemoveJunk(junk);
    }

    public void ConnectJunk(Junk from, Junk to)
    {
        if (from.color != to.color)
        {
            Debug.LogError($"Junk connection error: mismatched colors [{from.color},{to.color}]");
            return;
        }
        if (_junkGraphs == null )
        {
            AddJunk(from);
            AddJunk(to);
        } else
        {
            string warningMessage = "{0} being connected to {1} before color graph set up";
            if (!_junkGraphs.ContainsKey(from.color))
            {
                Debug.LogWarningFormat(warningMessage, from.color, to.color);    
                AddJunk(from);
            }

            if (!_junkGraphs.ContainsKey(to.color))
            {
                Debug.LogWarningFormat(warningMessage, from.color, to.color);    
                AddJunk(to);    
            }
        }

        Debug.Assert(_junkGraphs != null, nameof(_junkGraphs) + " != null");
        _junkGraphs[from.color].AddEdge(from,to);
    }

    public void DisconnectJunk(Junk from, Junk to)
    {
        if (from.color != to.color)
        {
            Debug.LogError($"Junk disconnection error: mismatched colors [{from.color},{to.color}]");
            return;
        }
        if (!_junkGraphs.ContainsKey(from.color))
        {
            // missing graph
            return;
        }
        _junkGraphs[from.color].RemoveEdge(from,to);
    }

    public List<Junk> GetTouching(Junk junk)
    {
        if ((_junkGraphs == null) || (!_junkGraphs.ContainsKey(junk.color)))
        {
            Debug.LogWarning($"GetTouching: Empty graph; {junk.color} is sole node. Creating new graph.");
            AddJunk(junk);
            return null;
        }
        return _junkGraphs[junk.color].GetConnectedJunks(junk);
    }
}
