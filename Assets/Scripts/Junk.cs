using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;

public enum JunkColor {
    Undefined,
    White,
    Red,
    Green,
    Blue,
    Magenta,
    Yellow,
}

public class Junk : MonoBehaviour
{
    [FormerlySerializedAs("Color")] public JunkColor color;
    [FormerlySerializedAs("_junkyard")] public Junkyard junkyard;
    [FormerlySerializedAs("_phase")] [SerializeField] private int phase;

    private Junkyard Junkyard
    {
        get
        {
            if (junkyard == null)
            {
                junkyard = RegisterWithJunkyard();
            }
            return junkyard;
        }
        set => junkyard = value;
    }

    // Start is called before the first frame update
    void Start()
    {
        
        SubscribeEvents();    
        // is the color of this junk defined?
        if (color == JunkColor.Undefined)
        {
            Debug.LogError($"Junk in {phase} has undefined color! Prefab might be messed up.");
        }
        
        Debug.LogFormat(
            "Materializing {0} junk: {1} with bounds {2}",
            Junk.JunkColorToString(color),
            gameObject,
            gameObject.GetComponent<Collider>().bounds
            );
    }

    void SubscribeEvents()
    {
        
        // AimEvent
        // get the raycast shooter to handle events
        var rayShooter = FindFirstObjectByType<RayShooter>();
        if (rayShooter)
        {
            rayShooter.OnAimStart += HandleAimStart;
            rayShooter.OnAimStop += HandleAimStop;
        }
        
        // DamageEvent
        var reactiveTarget = gameObject.GetComponent<ReactiveTarget>();
        if (reactiveTarget)
        {
            reactiveTarget.OnDamage += HandleDamage;
            reactiveTarget.OnDeath += HandleDeath;
        }
    }

    void UnsubscribeEvents()
    {
        var rayShooter = FindFirstObjectByType<RayShooter>();
        if (rayShooter)
        {
            rayShooter.OnAimStart -= HandleAimStart;
            rayShooter.OnAimStop -= HandleAimStop;
        }
        
        // DamageEvent
        var reactiveTarget = gameObject.GetComponent<ReactiveTarget>();
        if (reactiveTarget)
        {
            reactiveTarget.OnDamage -= HandleDamage;
            reactiveTarget.OnDeath -= HandleDeath;
        }
    }
    
    // Events
    

    /// <summary>
    /// Handle player starts aiming at the Junk.
    /// </summary>
    /// <param name="aimObject"></param>
    private void HandleAimStart(GameObject aimObject)
    {
        if (aimObject == gameObject)
        {
            /* if (junkyard == null)
            {
                Debug.Log($"junkyard is null for {gameObject.name}!");
                return;
            } // */
            string nullJunkyardMessage = $"{gameObject.name} junkyard is undefined...";
            Assert.IsNotNull(Junkyard, nullJunkyardMessage); 
            int touchingObjects = Junkyard.GetTouching(this).Count;            
            string aimingMessage = $"Player started aiming at {aimObject.name}, {touchingObjects} {color} touching";
            Debug.LogFormat(aimingMessage);
        }
    }

    /// <summary>
    /// Handle when the player stops aiming.
    /// </summary>
    /// <param name="aimObject"></param>
    private void HandleAimStop(GameObject aimObject)
    {
        if (aimObject == gameObject)
        {
            // List<GameObject> touchingObjects = Junkyard.GetTouchingObjects(gameObject);
            // Debug.LogFormat("Player stopped aiming at {0} and {1} touching objects", gameObject.name, touchingObjects.Count);
        }
    }
    
    private void HandleDamage(GameObject damagedObject)
    {
        if (damagedObject == gameObject)
        {
            Debug.LogFormat("{0} says ouch!", gameObject.name);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        Junk otherJunk = other.gameObject.GetComponent<Junk>();
        if (otherJunk)
        {
            string bumpMessage = $"{JunkColorToString(color)} bumped {JunkColorToString(otherJunk.color)}";
            Debug.Log(bumpMessage);
            /*
            if (otherJunk.color == color)
            {
                junkyard.ConnectJunk(this,otherJunk);
            } // */
        }
    }

    private void OnCollisionExit(Collision other)
    {
        Junk otherJunk = other.gameObject.GetComponent<Junk>();
        if (otherJunk)
        {
            string bumpMessage = $"{JunkColorToString(color)} unbumped {JunkColorToString(otherJunk.color)}";
            Debug.Log(bumpMessage);
            /*
            if (otherJunk.color == color)
            {
                junkyard.DisconnectJunk(this,otherJunk);
            } // */
        }
    }

    /// <summary>
    /// Handle when the junk is about to die.
    /// </summary>
    /// <param name="dyingObject"></param>
    private void HandleDeath(GameObject dyingObject)
    {
        if (dyingObject == gameObject)
        {
            Debug.LogFormat("{0} is going to die! Unsubscribing from events", gameObject.name);
            UnsubscribeEvents();
        }
    }

    // Update is called once per frame
    void Update()
    {
       // nothing yet 
    }
    
    /// <summary>
    /// Searches GameObjects tagged with "junkyard" and adds the Junk to the first Junkyard with a matching phase.
    /// </summary>
    /// <returns>Junkyard the Junk was added to, or none if none were found with a matching phase.</returns>
    Junkyard RegisterWithJunkyard()
    {
        string registerWithJunkyardsMessage = $"{gameObject.name} looking for junkyards...";
        Debug.Log(registerWithJunkyardsMessage);
        GameObject[] junkyards = GameObject.FindGameObjectsWithTag("junkyard");
        if (junkyards.Length == 0)
        {
            Debug.LogError(
                $"Couldn't register junk phase={phase}, color={Junk.JunkColorToString(color)}. Couldn't find any Junkyards!"
            );
            return null;
        }
        else
        {
            Debug.LogFormat("got some junkyards: {0}", junkyards.Length);
        }
        foreach (GameObject j in junkyards)
        {
            Junkyard jy = j.GetComponent<Junkyard>();
            if (jy == null)
            {
                Debug.LogError("Couldn't get Junkyard component from GameObject!");
            }
            if (phase != jy.phase) continue;
            jy.AddJunk(this);
            return jy;
        }
        Debug.LogErrorFormat(
            "Couldn't register junk phase={0}, color={1}. Check that the Junkyard tag and phase is set correctly!",
            phase,
            Junk.JunkColorToString(color)
            );
        return null;
    }


    // Helper methods
    public static string JunkColorToString(JunkColor clr)
    {
        switch (clr)
        {
            case JunkColor.White: return "white";
            case JunkColor.Blue: return "blue";
            case JunkColor.Green: return "green";
            case JunkColor.Magenta: return "magenta";
            case JunkColor.Yellow: return "yellow";
            case JunkColor.Red: return "red";
            case JunkColor.Undefined:
            default:
            {
                Debug.LogWarning("Junk has undefined color! Prefab might be incorrect.");
                return "undefined_color";
            }
        }
    }

}
