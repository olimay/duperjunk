using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
    public int startHealth = 5;
    private int _health;
    
    // Start is called before the first frame update
    void Start()
    {
        _health = startHealth;
    }

    // Update is called once per frame
    public void Hurt(int damage)
    {
        _health -= damage; // decrement health
        Debug.Log($"Health: {_health}"); // construct message by using string interpolation
    }
}
