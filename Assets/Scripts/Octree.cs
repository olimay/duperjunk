using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* In this updated Octree class, the `Insert` method checks the color of the GameObject being inserted using
 * `obj.GetComponent<Renderer>().material.color`. It only inserts the object if the color matches the `targetColor`
 * provided. The new method `QueryTouchingObjects` takes an input GameObject and recursively queries the Octree to find
 * all GameObjects of the same color that are touching it. It uses the `CheckCollision` method to determine if two
 * GameObjects are colliding. You can use the `QueryTouchingObjects` method to find all the GameObjects of the same
 * color touching a given input GameObject and iterate further to find other touching GameObjects recursively. Remember
 * to customize this code further based on your specific needs and modify the conditions for color comparison or
 * collision detection if necessary.
 */

public class Octree
{
    public class OctreeNode
    {
        public Bounds bounds;
        public bool isLeaf;
        public List<GameObject> objects;
        public OctreeNode[] children;

        public OctreeNode(Bounds bounds)
        {
            this.bounds = bounds;
            isLeaf = true;
            objects = new List<GameObject>();
            children = new OctreeNode[8];
        }
    }

    private OctreeNode _root;
    private int _maxDepth;

    public Octree(Bounds sceneBounds, int maxDepth)
    {
        _root = new OctreeNode(sceneBounds);
        this._maxDepth = maxDepth;
        Debug.LogFormat(
            _root != null
                ? "Created new octree with bounds={0} and maxDepth={1}"
                : "Something went wrong trying to create new octree with bounds={0} and maxDepth={1}",
            _root.bounds,
            maxDepth
        );
    }

    public void Insert(GameObject obj, Bounds objBounds, JunkColor targetColor)
    {
        bool success = false;
        if (obj.GetComponent<Junk>().color == targetColor)
        {
            success = InsertObject(_root, obj, objBounds);
        }
        Debug.Assert(
            success,
            string.Format(
                "Failed to insert object {0}, with bounds={1},color={2} into octree with target_color={3}!",
                obj,
                objBounds,
                Junk.JunkColorToString(obj.GetComponent<Junk>().color),
                Junk.JunkColorToString(targetColor)
                )
            ); 
    }

    private bool InsertObject(OctreeNode node, GameObject obj, Bounds objBounds)
    {
        if (!node.bounds.Intersects(objBounds))
        {
            Debug.LogFormat(
                "{0} with bounds {1} not in node's bounds of {2} so moving on",
                obj.name,
                objBounds,
                node.bounds
                );
            return false;
        }
        Debug.LogFormat(
            "{0} (bounds={1}) OK for node (bounds={2})",
            obj.name,
            objBounds,
            node.bounds
            );

        if (node.isLeaf && node.objects.Count < 8)
        {
            node.objects.Add(obj);
            Debug.LogFormat("{0} added to node {1}", obj.name, node);
            return true;
        }
        else
        {
            if (node.isLeaf)
            {
                Debug.LogFormat("need to split node {0}", node);
                SplitNode(node);
            }

            for (int i = 0; i < 8; i++)
            {
                if (node.children[i].bounds.Intersects(objBounds))
                {
                    if (InsertObject(node.children[i], obj, objBounds))
                        return true;
                }
            }
        }

        return false;
    }
    
    public void Remove(GameObject obj, Bounds objBounds)
    {
        RemoveObject(_root, obj, objBounds);
    }

    private bool RemoveObject(OctreeNode node, GameObject obj, Bounds objBounds)
    {
        if (!node.bounds.Intersects(objBounds))
            return false;

        bool removed = false;

        if (node.isLeaf)
        {
            removed = node.objects.Remove(obj);
        }
        else
        {
            for (int i = 0; i < 8; i++)
            {
                if (RemoveObject(node.children[i], obj, objBounds))
                {
                    removed = true;
                    break;
                }
            }
        }

        // Check if the node can be collapsed
        if (removed && node.isLeaf && node.objects.Count == 0 && CanCollapse(node))
        {
            CollapseNode(node);
        }

        return removed;
    }

    private bool CanCollapse(OctreeNode node)
    {
        for (int i = 0; i < 8; i++)
        {
            if (!node.children[i].isLeaf || node.children[i].objects.Count > 0)
            {
                return false;
            }
        }
        return true;
    }

    private void CollapseNode(OctreeNode node)
    {
        node.isLeaf = true;
        for (int i = 0; i < 8; i++)
        {
            node.children[i] = null;
        }
    }
    
    private void SplitNode(OctreeNode node)
    {
        node.isLeaf = false;
        Vector3 center = node.bounds.center;
        Vector3 halfSize = node.bounds.size * 0.5f;

        Bounds[] childBounds = new Bounds[8];
        for (int i = 0; i < 8; i++)
        {
            Vector3 offset = new Vector3(
                (i & 1) == 0 ? -halfSize.x : halfSize.x,
                (i & 2) == 0 ? -halfSize.y : halfSize.y,
                (i & 4) == 0 ? -halfSize.z : halfSize.z
            );
            childBounds[i] = new Bounds(center + offset, halfSize * 2);
            node.children[i] = new OctreeNode(childBounds[i]);
        }

        foreach (GameObject obj in node.objects)
        {
            Bounds objBounds = GetObjectBounds(obj);
            for (int i = 0; i < 8; i++)
            {
                if (node.children[i].bounds.Intersects(objBounds))
                {
                    InsertObject(node.children[i], obj, objBounds);
                }
            }
        }

        node.objects.Clear();
    }

    public List<GameObject> QueryTouchingObjects(GameObject inputObj, JunkColor targetColor)
    {
        List<GameObject> result = new List<GameObject>();
        QueryTouchingObjects(_root, inputObj, targetColor, result);
        Debug.LogFormat(
            "{0} is touching {1} other {2} objects",
            inputObj.name,
            result.Count,
            Junk.JunkColorToString(targetColor)
            );
        return result;
    }

    private void QueryTouchingObjects(OctreeNode node, GameObject inputObj, JunkColor targetColor, List<GameObject> result)
    {
        if (!node.bounds.Intersects(GetObjectBounds(inputObj))) return;
        foreach (GameObject obj in node.objects)
        {
            if (obj != inputObj && obj.GetComponent<Junk>().color == targetColor)
            {
                if (CheckCollision(inputObj, obj))
                {
                    result.Add(obj);
                    QueryTouchingObjects(node, obj, targetColor, result);
                }
            }
        }

        if (!node.isLeaf)
        {
            for (int i = 0; i < 8; i++)
            {
                QueryTouchingObjects(node.children[i], inputObj, targetColor, result);
            }
        }
    }

    private bool CheckCollision(GameObject obj1, GameObject obj2)
    {
        Collider coll1 = obj1.GetComponent<Collider>();
        Collider coll2 = obj2.GetComponent<Collider>();
        if (coll1 != null && coll2 != null)
        {
            if (coll1.bounds.Intersects(coll2.bounds))
            {
                Debug.LogFormat(
                    "Intersection: {0} {1} and {2} {3}",
                    obj1,
                    coll1.bounds,
                    obj2,
                    coll2.bounds);
                return true;
            }
            else
            {
                Debug.LogFormat(
                    "NO intersection: {0} {1} and {2} {3}",
                    obj1,
                    coll1.bounds,
                    obj2,
                    coll2.bounds);
                return false;
            }
        }
        Debug.LogErrorFormat("One of {0} and {1} have null colliders",obj1,obj2);
        return false;
    }

    private Bounds GetObjectBounds(GameObject obj)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        if (renderer != null)
        {
            return renderer.bounds;
        }

        Collider collider = obj.GetComponent<Collider>();
        if (collider != null)
        {
            return collider.bounds;
        }

        // If the object has no Renderer or Collider, return an empty bounds
        return new Bounds();
    }
}