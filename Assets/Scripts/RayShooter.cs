using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.PackageManager;
using UnityEngine;

public class RayShooter : MonoBehaviour
{
    private Camera _cam;
    // Start is called before the first frame update
    [SerializeField] private float mainShotDamage = 1.0f;
    [SerializeField] private GameObject altShotPrefab;
    [SerializeField] private float altShotLongevity = 1.0f;
   
    // Track targeting of objects
    private GameObject _currentAimObject;
    private RaycastHit _hitInfo;

    public delegate void AimEvent(GameObject aimObject);

    public event AimEvent OnAimStart;
    public event AimEvent OnAimStop;
    
    void Start()
    {
        _cam = GetComponent<Camera>();
        // hide the mouse cursor at the center of the screen
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void OnGUI()
    {
        int size = 12; // rough size of this font
        float posX = _cam.pixelWidth / 2 - size / 4;
        float posY = _cam.pixelHeight / 2 - size / 2;
        // display the text onscreen
        GUI.Label(new Rect(posX, posY, size, size), "*");
    }

    // Update is called once per frame
    void Update()
    {
        // Target using the exact center of the camera
        Vector3 point = new Vector3(_cam.pixelWidth / 2, _cam.pixelHeight / 2, 0);
        Ray ray = _cam.ScreenPointToRay(point);

        if (!Physics.Raycast(ray, out _hitInfo))
        {
            ClearAimObject();
            return;
        }
        // if the raycast hits a game object
        GameObject hitObject = _hitInfo.collider.gameObject;

        if (hitObject != _currentAimObject)
        {
            UpdateAimObject(hitObject);
        }

        if (Input.GetMouseButtonDown(0))
        {
            // retrieve the target the ray hit
            hitObject = _hitInfo.transform.gameObject;
            // check for the ReactiveTarget component on the object
            ReactiveTarget target = hitObject.GetComponent<ReactiveTarget>();
            if (target)
            {
                // Debug.Log($"Target hit {mainShotDamage} damage");
                target.ReactToHit(mainShotDamage);
            } else {
                StartCoroutine(HitIndicator(_hitInfo.point));
            }
            // Debug.Log("Hit " + hit.point);
        } else if (Input.GetMouseButtonDown(1))
        {
            // retrieve the target the ray hit
            hitObject = _hitInfo.transform.gameObject;
            ReactiveTarget target = hitObject.GetComponent<ReactiveTarget>();
            StartCoroutine(AltShotIndicator(_hitInfo.point));
        }
    }

    void ClearAimObject()
    {
        if (_currentAimObject)
        {
            OnAimStop?.Invoke(_currentAimObject);
            _currentAimObject = null;
        } 
    }

    /// <summary>
    /// Updates the object being actively aimed at.
    /// Triggers the event OnAimStop for the previously aimed at GameObject.
    /// Triggers the event OnAimStart for the currently aimed at GameObject, if it tagged as an AimableObject.
    /// </summary>
    /// <param name="hitObject">Object hit by Raycast (or other aiming method).</param>
    void UpdateAimObject(GameObject hitObject)
    {
        if (_currentAimObject)
        {
            OnAimStop?.Invoke(_currentAimObject);
        }
        
        if (hitObject.CompareTag("AimableObject"))
        {
            _currentAimObject = hitObject;
            OnAimStart?.Invoke(_currentAimObject);
        }
        else
        {
            _currentAimObject = null;
        }
    }
    
    private IEnumerator HitIndicator(Vector3 pos)
    {
        /*
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.position = pos;
        yield return new WaitForSeconds(1);
        Destroy(sphere);
        */
        yield return new WaitForSeconds(1);
    }

    private IEnumerator AltShotIndicator(Vector3 pos)
    {
        GameObject altShot;
        if (!altShotPrefab)
        {
            altShot = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        }
        else
        {
            altShot = Instantiate(altShotPrefab);
        }
        altShot.transform.position = pos;
        yield return new WaitForSeconds(altShotLongevity);
        Destroy(altShot);
    }
}
