using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactiveTarget : MonoBehaviour
{
    [SerializeField] private float deathDuration = 1.5f;
    [SerializeField] private float durability = 1.0f;

    public delegate void DamageEvent(GameObject dyingObject);
    public event DamageEvent OnDamage;
    public event DamageEvent OnDeath;
    
    public void ReactToHit(float damage)
    // method called by the shooting script
    {
        // WanderingAI behavior = GetComponent<WanderingAI>();
        // if (behavior) behavior.SetAlive(false);
        durability -= damage;
        TakeDamage();
        if (durability <= 0)
        {
            StartCoroutine(Die());
        }
    }

    void TakeDamage()
    // play sounds and animations here
    {
        // Debug.Log($"durability: {durability}");
        OnDamage?.Invoke(gameObject);
    }

    private IEnumerator Die()
    // show damage animation, then despawn the thing
    {
        Debug.Log($"{this.gameObject.ToString()} (id {this.gameObject.GetInstanceID()}) destroyed!");
        OnDeath?.Invoke(gameObject);
        yield return new WaitForSeconds(deathDuration);
        Destroy(this.gameObject); // a script can destroy itself
    }
}
