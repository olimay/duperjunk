using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JunkGraph : MonoBehaviour
{
    public JunkColor junkColor;
    private Dictionary<Junk, List<Junk>> _adjacencyList;

    public JunkGraph(JunkColor color)
    {
        _adjacencyList = new Dictionary<Junk, List<Junk>>();
    }

    public void AddJunk(Junk node)
    {
        if (node.color != junkColor)
        {
            string wrongColorError = $"Whoops, tried to add {Junk.JunkColorToString(node.color)} to " +
                                     $"{Junk.JunkColorToString(junkColor)} graph";
            Debug.LogError(wrongColorError);
            return;
        }
        if (!_adjacencyList.ContainsKey(node))
        {
            _adjacencyList[node] = new List<Junk>();
        }
    }

    public void RemoveJunk(Junk node)
    {
        _adjacencyList.Remove(node);

        foreach (var adjacentJunks in _adjacencyList.Values)
        {
            adjacentJunks.Remove(node);
        }
    }

    public void AddEdge(Junk from, Junk to)
    {
        if (_adjacencyList.ContainsKey(from) && _adjacencyList.ContainsKey(to))
        {
            if (!_adjacencyList[from].Contains(to))
            {
                _adjacencyList[from].Add(to);
            }
        }
    }

    public void RemoveEdge(Junk from, Junk to)
    {
        if (_adjacencyList.ContainsKey(from) && _adjacencyList.ContainsKey(to))
        {
            _adjacencyList[from].Remove(to);
        }
    }

    public List<Junk> GetConnectedJunks(Junk node)
    {
        if (_adjacencyList.ContainsKey(node))
        {
            return _adjacencyList[node];
        }
        return null;
    }
}
